(require 'package)
(package-initialize)

(if (not (display-graphic-p))
    (progn

      ;; Set some faces for htmlize (see
      ;; http://sebastien.kirche.free.fr/emacs_stuff/elisp/my-htmlize.el,
      ;; via https://stackoverflow.com/a/33687588/1814910)

      (custom-set-faces 
       '(default                      ((t (:foreground "#ffffff" :background "black"))))
       '(font-lock-builtin-face       ((t (:foreground "#ff0000"))))
       '(font-lock-comment-face       ((t (:bold t :foreground "#333300"))))
       '(font-lock-constant-face      ((t (:foreground "magenta"))))
       '(font-lock-function-name-face ((t (:bold t :foreground "Blue"))))
       '(font-lock-keyword-face       ((t (:foreground "yellow3"))))
       '(font-lock-string-face        ((t (:foreground "light blue"))))
       '(font-lock-type-face      ((t (:foreground "green"))))
       '(font-lock-variable-name-face ((t (:foreground "cyan" :bold t))))
       '(font-lock-warning-face       ((t (:foreground "red" :weight bold)))))

      (setq htmlize-use-rgb-map 'force)
))

;; (setf org-confirm-babel-evaluate nil)
;; no : inline-css as default is better
;;(setf org-html-htmlize-output-type 'css)
;; (setf org-export-time-stamp-file nil)
;; (setf org-export-with-author nil)
;; (setf org-export-with-creator nil)
;; (setf org-export-with-email nil)
;; (load-file "/emacs/ob-blockdiag.el")
;; (prefer-coding-system 'utf-8-unix)

;; Don't prompt for local variables, making them all safe
(setf enable-local-variables :all)

;; Load org-reveal exporter
(require 'ox-reveal)

;; Load org-ref
(require 'org-ref)

; https://stackoverflow.com/a/41625195
(setq org-latex-pdf-process
  '("lualatex -shell-escape -interaction nonstopmode %f"
    "lualatex -shell-escape -interaction nonstopmode %f"))

;; for now, don't set the default, relying on the local variable in the org source file
;; which means listings will be used by default
;(setq org-latex-src-block-backend 'minted)
;(setq org-latex-src-block-backend 'engraved)

; Silence compiler warnings as they can be pretty disruptive
(if (boundp 'comp-deferred-compilation)
    (setq native-comp-deferred-compilation nil
          native-comp-async-query-on-exit nil
          native-comp-async-report-warnings-errors 'silent
	  native-comp-speed -1))
;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent the use of stale byte-code. Otherwise, it saves us a little IO time
;; to skip the mtime checks on every *.elc file.
(setq load-prefer-newer noninteractive)
