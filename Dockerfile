# Inspired by https://github.com/binarin/docker-org-export
FROM debian:bookworm

ENV DEBIAN_FRONTEND noninteractive

RUN sed -i -e "s/ main/ main contrib/g" /etc/apt/sources.list.d/debian.sources
RUN apt-get update && apt-get -y upgrade

# Set the locale
RUN apt-get -y install locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Needed by startup.sh
RUN apt-get -y install sudo

# Add emacs + texlive stuff
RUN apt-get -y --no-install-recommends install ca-certificates emacs zip unzip
RUN apt-get -y --no-install-recommends install texlive-latex-recommended texlive-plain-generic texlive-fonts-recommended texlive-latex-extra
RUN apt-get -y --no-install-recommends install texlive-lang-french texlive-pstricks

# Use lualatex
RUN apt-get -y install texlive-luatex
# Add fonts like Verdana
RUN apt-get -y --no-install-recommends install ttf-mscorefonts-installer

RUN apt-get -y --no-install-recommends install git

WORKDIR /emacs
ENV HOME /emacs

# Some Emacs packages installation (org-mode, etc.)
ADD install.el /emacs/install.el
RUN emacs --batch --load install.el

# The script is the exporter
ADD export.el /emacs/export.el

ENV SHELL=/bin/bash
# Defaults to be overruled by launch script (docker run ... -e USERID=1001 ...)
ENV USER=ubuntu
ENV USERID=1000

ADD startup.sh /startup.sh
ENTRYPOINT ["/startup.sh"]

