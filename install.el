(require 'package)
;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-initialize)
;; install version of org-mode >= 9.6 to be able to use org-latex-src-block-backend for engraved
;; work around the problem of org-mode being a builtin :-/
;; see https://github.com/jwiegley/use-package/issues/319#issuecomment-845214233
(assq-delete-all 'org package--builtins)
(assq-delete-all 'org package--builtin-versions)
(package-install 'org)
(package-install 'engrave-faces)
(package-install 'org-contrib)
(package-install 'htmlize)
;(package-install 'json-mode)
;(package-install 'erlang)
(package-install 'ox-reveal)

;; not using org-beautify as it renders better with X. Instead use custom faces (see export.el)
;(package-install 'org-beautify-theme)
;(load-theme 'org-beautify t)

(package-install 'org-ref)

;; install php-mode which we use a lot in CSC4101 at Telecom SudParis
(package-install 'php-mode)
